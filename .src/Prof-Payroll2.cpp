#include<iostream>
using namespace std;

class Employee {

	public:
//Not a default constructor
		Employee(int n) {
			cout << "\nEmployee One Parameter Connstructor" << endl;
			DepartmentNumber = n;
		}
		Employee() {
			cout << "\nEmployee Default Constructor" << endl;
		}
		int GetName() {
			cout << "Input Emp Last Name: ";
			cin >> LastName;
			return 0;
		}
	protected:
		long int myIdNumber;
		string LastName, Firstnme;
		char MiddleInitial;
		int DepartmentNumber;

};

class SalariedEmployee : public Employee {
	public:
		//Salaried Employee Methods
		SalariedEmployee(int n) : Employee(
			    n) {  } //Explicitly invoke the one parameter base class constructor
		SalariedEmployee() {
			cout << "\tSalaried Employee Constructor (derived class)" << endl;
		}
	protected:
		float mySalary;
};

class HourlyEmployee : public Employee {
	public:
		//Hourly Employee Methods
		HourlyEmployee(int n) : Employee(
			    n) {   } //Explicitly Invoke the one parameter base class constructor
		HourlyEmployee() {
			cout << "\tHourly Employee Constructor (derived class)" << endl;
		}
	protected:
		float myHourlyWage;
		float HoursWorked;
};

class ExecutiveEmployee : public SalariedEmployee {
	public:
		//Executive Employee Methods
		ExecutiveEmployee(int n) : SalariedEmployee(
			    n) {   } //Explicitly Invoke the one parameter SalariedEmployee constructor
		ExecutiveEmployee() {
			cout << "\t\tExecutive Employee Constructor (derived class)" << endl;
		}
	protected:
		float myExecPay;
};

int main() {
//Create some objects here:

	Employee E1(5);
	SalariedEmployee SE1(6), SE2;
	HourlyEmployee HE1(7);
	ExecutiveEmployee EE1(8);
	SE2.GetName();

	return 0;
}
