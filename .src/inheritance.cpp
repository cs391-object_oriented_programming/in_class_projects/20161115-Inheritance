#include <iostream>
#include <vector>
using namespace std;

class Employee {
	public:
		void setName() {
			cout<<"\n>>Enter first name: ";
			cin>>firstName;
			cout<<">>Enter last name: ";
			cin>>lastName;
			cout<<">>Enter middle initial: ";
			cin>>middleInitial;
		}

		void setId() {
			cout<<"\n>>Enter employeee ID number: ";
			cin>>idNumber;
		}

		void setWeeklyPay(float pay) {
			weeklyPay= pay;
		}

		void getPay() {
			printf("\n (%d), %s %s worked %d hours and %d minutes. Final pay: $%.2f.\n"
			       ,idNumber,firstName.c_str(),lastName.c_str(),hoursWorked,minutesWorked,weeklyPay);
		}

	protected:
		long idNumber;
		string lastName, firstName;
		string middleInitial;
		int departmentNumber;

		/*wages*/
		float payRate;
		int hoursWorked;
		int minutesWorked;
		float weeklyPay;
};

class SalariedEmployee : public Employee {
	public:

	protected:
		float salary;
};

class HourlyEmployee : public Employee {
	public:
		void setPay() {
			cin.get();
			cout<<"\n>>Enter hourly pay rate: ";
			cin>>payRate;
			cout<<">>Enter hours worked(no minutes): ";
			cin>>hoursWorked;
			cout<<">>Enter minutes worked: ";
			cin>>minutesWorked;
		}

		float computeWeeklyWages() {
			float decimalMinutes= (float)minutesWorked/60;
			float comboHM= hoursWorked + decimalMinutes;

			float regTime;
			float xtraTime;
			float xtraTimeRate= 1.5;
			float fullPay;
			if(comboHM>40.00) {
				regTime= 40;
				xtraTime= comboHM - regTime;
				weeklyPay= (regTime*payRate) + (xtraTime*xtraTimeRate);

			} else	weeklyPay= comboHM*payRate;

			return weeklyPay;
		}
	protected:
};

class ExecutiveEmployee : public SalariedEmployee {
	public:

	protected:
		float execPay;
};

int main() {
	HourlyEmployee he1,he2,he[10];

	he[0].setName();
	he[0].setId();
	he[0].setPay();
	he[0].setWeeklyPay(he[0].computeWeeklyWages());
	he[0].getPay();

//	delete he1;
	return 0;
}
